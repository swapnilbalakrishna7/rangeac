# README #

To run the application run the ZipCodeRange.java file

Sample Input and Output :

Input : 
[94133, 94133]
[94200, 94299]
[94600, 94699]

Output :
[94133, 94133]
[94200, 94299]
[94600, 94699]


Input :
[94100, 94107]
[94105, 94250]
[94600, 94699]

Output :
[94100, 94250]
[94600, 94699]

To run the test cases run the ZipCodeRangeTest.java file