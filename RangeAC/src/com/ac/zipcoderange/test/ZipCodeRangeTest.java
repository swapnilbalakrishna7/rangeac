/**
 * 
 */
package com.ac.zipcoderange.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.ac.zipcoderange.Range;
import com.ac.zipcoderange.ZipCodeRange;

/**
 * @author swapnilbalakrishna7
 *
 */
public class ZipCodeRangeTest {

	List<Range> expectedResult;
	ZipCodeRange zipCodeRange = new ZipCodeRange();

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		expectedResult = new ArrayList<Range>();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		expectedResult.clear();
		zipCodeRange.clearRangeList();
	}
	
	@Test
	public void testValidRanges() {
		try {
			setUp();
			Range range1 = new Range(94133, 94133);
			Range range2 = new Range(10, 94299);
			Range range3 = new Range(94600, 94699);

			zipCodeRange.addNewRange(range1);
			zipCodeRange.addNewRange(range2);
			zipCodeRange.addNewRange(range3);

			Assert.assertEquals("InvalidRange", 2,
					zipCodeRange.getListOfRanges().size());

			tearDown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testZipRanges1() {
		try {
			setUp();
			Range range1 = new Range(94133, 94133);
			Range range2 = new Range(94200, 94299);
			Range range3 = new Range(94600, 94699);

			zipCodeRange.addNewRange(range1);
			zipCodeRange.addNewRange(range2);
			zipCodeRange.addNewRange(range3);

			Range range4 = new Range(94133, 94133);
			Range range5 = new Range(94200, 94299);
			Range range6 = new Range(94600, 94699);

			expectedResult.add(range4);
			expectedResult.add(range5);
			expectedResult.add(range6);

			Assert.assertEquals("Success", expectedResult,
					zipCodeRange.getListOfRanges());

			tearDown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testZipRanges2() {
		try {
			setUp();
			Range range1 = new Range(94100, 94107);
			Range range2 = new Range(94105, 94250);
			Range range3 = new Range(94600, 94699);

			zipCodeRange.addNewRange(range1);
			zipCodeRange.addNewRange(range2);
			zipCodeRange.addNewRange(range3);

			Range range4 = new Range(94100, 94250);
			Range range5 = new Range(94600, 94699);

			expectedResult.add(range4);
			expectedResult.add(range5);

			Assert.assertEquals("Success", expectedResult,
					zipCodeRange.getListOfRanges());

			tearDown();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
