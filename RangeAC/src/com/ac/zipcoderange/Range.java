/**
 * 
 */
package com.ac.zipcoderange;

/**
 * @author swapnilbalakrishna7
 *
 */
public class Range {

	private int lowerBound;
	private int upperBound;

	public Range(int lowerBound, int upperBound) {
		// Correctly set the upper and lower bound
		if (lowerBound <= upperBound) {
			this.lowerBound = lowerBound;
			this.upperBound = upperBound;
		} else {
			this.lowerBound = upperBound;
			this.upperBound = lowerBound;
		}
	}

	/**
	 * @return the lowerBound
	 */
	public int getLowerBound() {
		return lowerBound;
	}

	/**
	 * @param lowerBound
	 *            the lowerBound to set
	 */
	public void setLowerBound(int lowerBound) {
		this.lowerBound = lowerBound;
	}

	/**
	 * @return the upperBound
	 */
	public int getUpperBound() {
		return upperBound;
	}

	/**
	 * @param upperBound
	 *            the upperBound to set
	 */
	public void setUpperBound(int upperBound) {
		this.upperBound = upperBound;
	}

	public boolean equals(Object o) {
		boolean result = false;
		if (o instanceof Range) {
			Range r = (Range) o;
			result = (this.getLowerBound() == r.getLowerBound() && this
					.getUpperBound() == r.getUpperBound());
		}
		return result;
	}

	@Override
	public String toString() {
		return "Range [lowerBound=" + lowerBound + ", upperBound=" + upperBound
				+ "]";
	}

}
