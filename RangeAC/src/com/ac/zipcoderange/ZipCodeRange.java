package com.ac.zipcoderange;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * @author swapnilbalakrishna7
 *
 */

public class ZipCodeRange {

	private List<Range> listOfRanges = new ArrayList<Range>();

	public void printRanges() {
		if (listOfRanges.size() > 0) {
			for (Range range : listOfRanges) {
				System.out.println(range.toString());
			}
		}
	}

	public List<Range> getListOfRanges() {
		return this.listOfRanges;
	}

	public void clearRangeList() {
		this.listOfRanges.clear();
	}

	public boolean addNewRange(Range rangeToAdd) {
		if (!checkValidRange(rangeToAdd))
			return false;
		if (listOfRanges.size() == 0)
			listOfRanges.add(rangeToAdd);
		else {
			ListIterator<Range> iterator = listOfRanges.listIterator();
			boolean addRange = true;
			while (iterator.hasNext()) {
				Range range = iterator.next();
				// If the new range within existing range we don't add to the
				// list
				if (range.getLowerBound() <= rangeToAdd.getLowerBound()
						&& range.getUpperBound() >= rangeToAdd.getUpperBound()) {
					addRange = false;
				}
				// If the new range is out of existing range we add to the list
				else if (range.getLowerBound() > rangeToAdd.getUpperBound()
						|| range.getUpperBound() < rangeToAdd.getLowerBound()) {
					addRange = true;
				}
				// If the range overlaps we update the lowerBound/upperBound
				else {
					if (range.getLowerBound() < rangeToAdd.getLowerBound())
						rangeToAdd.setLowerBound(range.getLowerBound());
					if (range.getUpperBound() > rangeToAdd.getUpperBound())
						rangeToAdd.setUpperBound(range.getUpperBound());
					iterator.remove();
					addRange = true;
				}
			}
			if (addRange)
				listOfRanges.add(rangeToAdd);
		}
		return true;
	}

	public boolean checkValidRange(Range range) {
		if (range.getLowerBound() < 10000 || range.getUpperBound() > 99999) {
			System.err.print("Invalid zipcode range [" + range.getLowerBound()
					+ "," + range.getUpperBound() + "] -- Should be 5 digits");
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

		ZipCodeRange zipCodeRange = new ZipCodeRange();
		Range range1 = new Range(94133, 94133);
		Range range2 = new Range(94133, 94200);
		Range range3 = new Range(94600, 94699);

		zipCodeRange.addNewRange(range1);
		zipCodeRange.addNewRange(range2);
		zipCodeRange.addNewRange(range3);
		zipCodeRange.printRanges();
	}
}
